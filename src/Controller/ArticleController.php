<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;





class ArticleController extends AbstractController
{

    /**
     * @Route("/article", name="article")
     */
    public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/ArticleController.php',
        ]);
    }

    /**
     * @Rest\Post("/articles")
     */
    public function createAction(Request $request, ValidatorInterface $validator)
    {
        header("Access-Control-Allow-Origin: *");

        $article = new Article();
        $data = json_decode($request->getContent(), true);
        $form =  $this->createForm( ArticleType::class, $article );
        $form->submit($data);
        $article = $form->getData();

        $errors = $validator->validate($article);

        if (count($errors) > 0) {
            return $this->json("Erreur ", RESPONSE::HTTP_BAD_REQUEST);
        }
        $entity_manager = $this->getDoctrine()->getManager();
        $entity_manager->persist($article);
        $entity_manager->flush();
        return $this->json($article , 201);
    }

    /**
     * @Rest\Get("/articles")
     */
    public function getArticlesAction(Request $request, SerializerInterface $serializer)
    {
        header("Access-Control-Allow-Origin: *");

        $repository = $this->getDoctrine()->getRepository(Article::class);
        return $this->json( $serializer->serialize($repository->findAll() , "json"), 200);
    }

    /**
     * @Rest\Get("/articles/{slug}")
     */
    public function getArticleAction(Request $request, SerializerInterface $serializer)
    {
        header("Access-Control-Allow-Origin: *");

        $repository = $this->getDoctrine()->getRepository(Article::class);
        $article = $repository->findOneBy(["slug" => $request->get('slug')]);
        if( isset($article))
            return $this->json( $serializer->serialize($article , "json"), 200);
        else
            return $this->json( "", RESPONSE::HTTP_NOT_FOUND);
    }

    /**
     * @Rest\Delete("/articles/{id}")
     */
    public function removeArticlesAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Article::class);
        $toRemove = $repository->find($request->get('id'));
        if(!isset($toRemove)) {
            return $this->json("Erreur, ressource non trouvée", 400);
        }

        $entity_manager = $this->getDoctrine()->getManager();
        $entity_manager->remove($toRemove);
        $entity_manager->flush();

        return $this->json("", 204);
    }

    /**
     * @Rest\Put ("/articles/{slug}")
     */
    public function updateArticlesAction(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {

        $repository = $this->getDoctrine()->getRepository(Article::class);
        $article = $repository->findOneBy(["slug" => $request->get('slug')]);
        if(!isset($article))
            return  $this->json(['message' => 'Article non trouvé'], Response::HTTP_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        $form =  $this->createForm( ArticleType::class, $article );
        $form->submit($data);
        $article = $form->getData();
        $errors = $validator->validate($article);
        if (count($errors) > 0) {
            return $this->json("Erreur ", RESPONSE::HTTP_BAD_REQUEST);
        }

        $entity_manager = $this->getDoctrine()->getManager();
        $entity_manager->persist($article);
        $entity_manager->flush();

        return $this->json($serializer->serialize($article , "json"), 201);

    }

}
